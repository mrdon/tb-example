from .cmd_echo import EchoController


def load(app):
    app.handler.register(EchoController)
