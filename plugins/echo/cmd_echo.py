from cement import Controller, ex
from tb.arguments import repository
from tb import Tb, TbError

from .util import echo_info


class EchoController(Controller):
    class Meta:
        label = 'echo'
        stacked_on = 'base'
        stacked_type = 'nested'
        description = "Echos the name and current repository"
        arguments = repository.arguments + [
            (['--name'],
             dict(help='the name of the user',
                  action='store',
                  default='Foo',
                  dest='name')),
        ]

    @ex(help="echos the name and repository")
    @repository.validate
    def echo(self):
        app: Tb = self.app
        if self.app.pargs.repository not in app.repositories:
            raise TbError("No known repository selected or detected")
        repo = app.repositories[self.app.pargs.repository]

        name = self.app.pargs.name

        echo_info(app, name, repo.name)

    def _default(self):
        self.echo()
